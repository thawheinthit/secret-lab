<?php

namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Database\Factories\ContentFactory;
use App\Modules\Master\Models\Content;
use App\Modules\Master\Repositories\Content\DbContentRepository;



class ContentTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Can show content by key
     *
     * @return void
     */

    public function test_can_show_content_by_key()
    {
        //save first
        $fakeContent = Content::factory()->create();

        $contentRepo = new DbContentRepository();
        $respond = $contentRepo->save($fakeContent);

        
        //then find
        $respond = $contentRepo->find($fakeContent->key);
        $this->withoutExceptionHandling();

        //expect the key
        $this->assertEquals($fakeContent->key, $respond->key);
        // $this->assertEquals('value1', $respond->value);

    }

    /**
     * Can show content
     *
     * @return void
     */

    public function test_can_show_all_content()
    {
        //save first
        Content::factory()->count(3)->create();
        $contentRepo = new DbContentRepository();
       
        //then find
        $respond = $contentRepo->all();
        $this->withoutExceptionHandling();
        
        //expect the key
        $this->assertEquals(3, count($respond));

    }
}
