<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Modules\Master\Models\Content;
use Tests\TestCase;
use Database\Factories\ContentFactory;

use App\Modules\Master\Repositories\Content\DbContentRepository;

class ContentApiTest extends TestCase
{
    use RefreshDatabase;

    protected $sampleData = [
        'mykey' => "value1"
    ];

    protected $sampleKey = 'mykey';

    /**
     * Can create new content
     *
     * @return void
     */
    public function test_can_create_new_content()
    {

        // $this->withoutExceptionHandling();
        $this->json('POST', 'api/contents', $this->sampleData, ['Accept' => 'application/json'])
            ->assertStatus(200);


    }

    /**
     * Can show error without data
     *
     * @return void
     */
    public function test_show_error_without_data()
    {
        $assertJson = [
            "error" => [
                "message" => "Validation Error!",
                "status_code" => 422,
                "validateError" => [
                    "key" => ["The key field is required."],
                    "value" => ["The value field is required."]
                ]
            ]
        ];

        $this->json('POST', 'api/contents', [], ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJson($assertJson,$strict = false);
    }
    
    /**
     * Can retrieve content by key
     *
     * @return void
     */
    public function test_can_retrieve_content_by_key()
    {

        $this->json('POST', 'api/contents', $this->sampleData, ['Accept' => 'application/json'])
            ->assertStatus(200);


        $this->json('GET', 'api/contents/' . $this->sampleKey, ['Accept' => 'application/json'])
            ->assertStatus(200);

    }

    /**
     * Can retrieve all content
     *
     * @return void
     */
    public function test_can_retrieve_all_content()
    {
        $this->json('GET', 'api/contents', ['Accept' => 'application/json'])
            ->assertStatus(200);

    }

    /**
     * Can retrieve content by key and timestamp
     *
     * @return void
     */
    public function test_can_retrieve_content_by_key_and_timestamp()
    {
        $this->withoutExceptionHandling();
        $postRespond = $this->json('POST', 'api/contents', $this->sampleData, ['Accept' => 'application/json']);
        $postRespond->assertStatus(200);

        //need to assert with timestamp
        // $this->json('GET', 'api/contents/' . $this->sampleKey . "?timestamp=", ['Accept' => 'application/json'])
        //     ->assertStatus(200);


        //should show not found status
        $this->json('GET', 'api/contents/' . $this->sampleKey . "?timestamp", ['Accept' => 'application/json'])
            ->assertStatus(200);

    }




    
}
