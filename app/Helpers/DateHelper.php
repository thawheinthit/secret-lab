<?php
// date_default_timezone_set('UTC');
namespace App\Helpers;

use Carbon\Carbon;

class DateHelper
{

    public static function getTimeOnly($date_time) : string
    {
        if(!isset($date_time))
            return "";

        try {
            return date("H:ia", strtotime($date_time));
        }
        catch(Exception $e)
        {
            return "01/01/1990";
        }
    }


    public static function dateToTimeStamp($date) : string
    {
        if(!isset($date))
            return "";

        try {
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
        }
        catch(Exception $e)
        {
            return "01/01/1990";
        }
    }


    public static function timeStampToDate($timeStamp) : string
    {

        if(!isset($timeStamp))
            return "";

        try {
            return Carbon::createFromTimestamp($timeStamp);
        }
        catch(Exception $e)
        {
            return "01/01/1990";
        }
    }
}
