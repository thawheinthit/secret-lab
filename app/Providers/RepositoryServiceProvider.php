<?php

namespace App\Providers;
use App\Modules\Master\Repositories\Content\DbContentRepository;
use App\Modules\Master\Repositories\Content\ContentRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bind the interface to an implementation repository class
     */
    public function register()
    {
        $this->app->bind(
            ContentRepository::class,
            DbContentRepository::class
        );
    }
}