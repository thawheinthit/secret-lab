<?php
namespace App\Modules\Master\Repositories\Content;

interface ContentRepository
{
    public function paginate($sortQueriesData);
    public function find($id);
    public function validate($data);
    public function save($data);
}