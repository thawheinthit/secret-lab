<?php
namespace App\Modules\Master\Repositories\Content;

use App\Main\DbRepository;
use App\Modules\Master\Models\Content;
use App\Helpers\DateHelper;

use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Config;
class DbContentRepository extends DbRepository implements ContentRepository {
    
    public function all() {
        return Content::all(); 
    }

    public function paginate($per_page = 5){
        return Content::orderBy('created_at', 'asc')->paginate($per_page);
    }

    public function validate($data) {
        $rules = array(
            'key' => 'required',
            'value' => 'required'
        );
       
        return Validator::make($data, $rules);
    }


    public function find($key) {
        $cacheKey = Config::get('cache.prefix') . $key;
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $maxRecord = Content::where('key', $key)->max('id');
        return Content::find($maxRecord);
    }

    public function findWithTimeStamp($key,$timeStamp){
        $cacheKey = Config::get('cache.prefix') . $key;
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }
        return Content::where('key', $key)->where('created_at',DateHelper::timeStampToDate($timeStamp))->first();
    }

    
    public function save($data){
        $content = new Content();
        foreach ($data as $key => $value) {
            $content->key = $key;
            $content->value = $value;
        }
        $content->save();
        Cache::put($content->key, $content->value, $seconds = 60);
        return $content;

    }


}