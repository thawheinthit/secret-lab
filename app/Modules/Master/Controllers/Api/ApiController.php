<?php namespace App\Modules\Master\Controllers\Api;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;


class ApiController extends BaseController {
	protected $statusCode;
    

	public function getStatusCode()
	{
		return $this->statusCode;
	}

	public function setStatusCode($statusCode)
	{
		$this->statusCode = $statusCode;
		return $this;
	}

	public function respond($data, $headers = [])
	{
		return response()->json($data, $this->getStatusCode(), $headers);
	}

	public function respondWithPagination($items, $data)
	{
		$data = array_merge($data, [
			'paginator' => [
                'total_count' => $items->total(),
                'total_page'  => ceil($items->total() / $items->perPage()),
                'current_page'=> $items->currentPage(),
                'limit'       => $items->perPage()
            ]
		]);
		return $this->respond($data);
	}
	
	public function respondWithError($message)
	{
		return $this->respond([
			'error' => [
				'message' => $message,
				'status_code' => $this->getStatusCode()
			]
		]);
	}

	public function respondWithValidationError($message, $vadateMessage)
	{
		return $this->respond([
			'error' => [
				'message' => $message,
				'status_code' => $this->getStatusCode(),
				'validateError' => $vadateMessage
			]
		]);
	}

	/**
	 * @param $message
	 * @return mixed
	 */

	public function respondCreated($message, $data)
	{
		return $this->setStatusCode(201)->respond([
			'message' => $message,
			'data' => $data
		]);
	}

	public function respondUpdated($message, $data)
	{
		return $this->setStatusCode(201)->respond([
			'message' => $message,
			'data' => $data
		]);
	}

	public function respondDeleted($message, $data)
	{
		return $this->setStatusCode(201)->respond([
			'message' => $message,
			'data' => $data
		]);
	}

	public function respondNoRecord()
	{
		return $this->setStatusCode(204)->respond([]);
	}


	/**
	 * @param $message
	 * @return mixed
	 */
	public function respondNotFound($message = 'Not found!')
	{
		return $this->setStatusCode(404)->respondWithError($message);
	}
	
	/**
	 * @param $message
	 * @return mixed
	 */
	public function respondPostError($message = 'Validation Error!')
	{
		return $this->setStatusCode(422)->respondWithError($message);
	}

	/**
	 * @param $message
	 * @return mixed
	 */
	public function respondPostValidationError($vadateMessage)
	{
		return $this->setStatusCode(422)->respondWithValidationError('Validation Error!', $vadateMessage);
	}

	/**
	 * @param $message
	 * @return mixed
	 */

	public function respondInternalError($message = 'Internal Error!')
	{
		return $this->setStatusCode(500)->respondWithError($message);
	}

	/**
	 * @param $message
	 * @return mixed
	 */

	public function respondInvalid($message = 'Invalid!')
	{
		return $this->setStatusCode(400)->respondWithError($message);
	}




}