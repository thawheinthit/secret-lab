<?php

namespace App\Modules\Master\Controllers\Api;

use App\Modules\Master\Repositories\Content\ContentRepository;
use App\Modules\Master\ApiTransformation\Content as ContentApi;
use App\Modules\Master\ApiTransformation\ContentCollection as ContentCollectionApi;

use Illuminate\Http\Request;
use App\Helpers\DateHelper;

class ContentController extends ApiController
{
    protected $per_page = 20;
    protected $contentRepo;
    
    public function __construct(ContentRepository $contentRepo) {
        $this->contentRepo = $contentRepo;
        $this->middleware('api');
    }

    public function index(){
        return new ContentCollectionApi($this->contentRepo->paginate($this->per_page));
    }

    public function show($key,Request $request){
        
        //test _> 1650003211
        if($request->has('timestamp')) {
            $records = $this->contentRepo->findWithTimeStamp($key,$request->input('timestamp'));
            if($records){
                return new ContentApi($this->contentRepo->findWithTimeStamp($key,$request->input('timestamp')));    
            }else{
                return $this->respondNoRecord();
            }
            
        }
        // return $this->contentRepo->find($key);
        return new ContentApi($this->contentRepo->find($key));
    }
    
    public function store(Request $request) 
    {
        $requestData = [];

        foreach ($request->all() as $key => $value) {
            $requestData['key'] = $key;
            $requestData['value'] = $value;
        }
        $validator = $this->contentRepo->validate($requestData);
        
        if($validator->fails()){
            return $this->respondPostValidationError($validator->messages());
        }

        $insertedContent = $this->contentRepo->save($request->all());
        return DateHelper::dateToTimeStamp($insertedContent->created_at);

        // return DateHelper::getTimeOnly($insertedContent->created_at);

        
    }
}
