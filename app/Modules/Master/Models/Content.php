<?php

namespace App\Modules\Master\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Database\Factories\ContentFactory;

class Content extends Model
{
    use HasFactory;

    protected static function newFactory()
    {
        return ContentFactory::new();
    }
}
