<?php

namespace App\Modules\Master\ApiTransformation;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Helpers\DateHelper;

class Content extends JsonResource
{
    public static $wrap = null;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $response = [
            'value' => $this->value
        ];

        
        return $response;
    }
}
