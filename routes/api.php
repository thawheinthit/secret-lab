<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Modules\Master\Controllers\Api as ApiControllers;
use App\Http\Middleware\SecretTokenIsValid;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::get('generate',function() {
//     return App\Helpers\DateHelper::dateToTimeStamp("2022-04-16 05:05:59");
// });

// Route::resource('contents', 'App\Modules\Master\Controllers\Api\ContentController');


Route::post('/contents', [App\Modules\Master\Controllers\Api\ContentController::class, 'store']);

Route::get('/contents', [App\Modules\Master\Controllers\Api\ContentController::class, 'index'])->middleware(SecretTokenIsValid::class);

Route::get('/contents/{key}', [App\Modules\Master\Controllers\Api\ContentController::class, 'show'])->middleware(SecretTokenIsValid::class);


