<?php

namespace Database\Factories;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContentFactory extends Factory
{
    protected $model = \App\Modules\Master\Models\Content::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'key' => $this->faker->title,
            'value' => $this->faker->safeEmail
        ];
    }
}
